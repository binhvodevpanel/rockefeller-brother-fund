<?php

/**
 * @file CLI script to allow a developer to obtain the GivingData OpenIDConnect authentication
 * token.
 */

require __DIR__ . '/vendor/autoload.php';
use Jumbojett\OpenIDConnectClient;

$oidc = new OpenIDConnectClient('https://rbf.givingdata.com',
                                'GivingDataPublishedGrants',
                                'grantsdata');
$oidc->providerConfigParam(['token_endpoint' => 'https://rbf.givingdata.com/identity/core/connect/token']);
$oidc->addScope(array('openid', 'email', 'GivingDataApi'));

//Add username and password
$oidc->addAuthParam(array('username'=>'jeff@devcollaborative.com'));
$oidc->addAuthParam(array('password'=>'G&CB$xw8Hd0G'));

//Perform the auth and return the token (to validate check if the access_token property is there and a valid JWT) :
$token = $oidc->requestResourceOwnerToken(TRUE)->access_token;
var_export($token);
