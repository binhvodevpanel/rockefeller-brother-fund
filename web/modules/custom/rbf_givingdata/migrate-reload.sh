#!/usr/bin/php -q

<?php
shell_exec('lando drush cim --partial --source="modules/custom/rbf_givingdata/config/install"');
shell_exec('lando drush migrate:rollback rbf_givingdata_grants_node');
shell_exec('lando drush migrate:import rbf_givingdata_grants_node');