<?php

namespace Drupal\rbf_givingdata\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "months_to_years"
 * )
 *
 * Convert a month value to years and use the following:
 *
 * @code
 * field_years:
 *   plugin: months_to_years
 *   source: integer
 * @endcode
 *
 */

class MonthsToYears extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_integer($value)) {
      return round($value/12);
    } else {
      return null;
    }
  }
}

