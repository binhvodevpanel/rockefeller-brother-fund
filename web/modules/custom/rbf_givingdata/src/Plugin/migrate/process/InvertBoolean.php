<?php

namespace Drupal\rbf_givingdata\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "invert_boolean"
 * )
 *
 * Make true false and make false true:
 *
 * @code
 * field_years:
 *   plugin: invert_boolean
 *   source: integer
 * @endcode
 *
 */

class InvertBoolean extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_bool($value)) {
      if ($value) {
        return false;
      } else {
        return true;
      }
    }
  }
}

