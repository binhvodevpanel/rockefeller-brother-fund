<?php

namespace Drupal\rbf_givingdata\Plugin\migrate\process;

use League\ISO3166\ISO3166DataProvider;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "country_code_3_to_2"
 * )
 *
 * Convert a 3-digit country code to a 2-digit code:
 *
 * @code
 * field_country_code:
 *   plugin: country_code_3_to_2
 *   source: string
 * @endcode
 *
 */

class CountryCode3To2 extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_string($value) && strlen($value) == 3) {
      // No official ISO/script support for Kosovo value of XKX yet, so let's
      // handle via custom code.
      if ($value == 'XKX') {
        return 'XK';
      }
      $data = (new \League\ISO3166\ISO3166)->alpha3($value);
      return($data['alpha2']);
    } else if ($value == NULL) {
      return NULL;
    } else {
      \Drupal::logger('rbf_givingdata')->warning($row->get('gd_id') . ': Country Code must be a ISO 3-character string');
      return NULL;
    }
  }
}

