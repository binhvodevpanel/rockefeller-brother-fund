<?php

namespace Drupal\rbf_givingdata\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateException;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "convert_gd_attributes"
 * )
 *
 * Use the GivingData-imported program budget and program goal attributes
 * to identify the appropriate program strategy or program relationship value:
 *
 * @code
 * field_program_strategy:
 *   plugin: convert_gd_attributes
 *   source: ['program_budget', 'program_goal']
 *   type: program_strategy (or program)
 * @endcode
 *
 */

class ConvertGDAttributes extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */

  private function find_term_name_from_id($id) {
    $term_query = \Drupal::entityQuery('taxonomy_term')->condition('tid', $id);

    $result = reset($term_query->execute());
    if ($result) {
      $term = \Drupal\taxonomy\Entity\Term::load($result);
      return $term->getName();
    } else {
      return 'UNKNOWN';
    }
  }

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $program_budget = $value[0];
    $program_goal = $value[1];

    $type = $cake_type = $this->configuration['type'];

    $query = \Drupal::entityQuery('paragraph')
      ->condition('type', 'gd_taxonomy_conversion')
      ->condition('field_gd_program_budget_source', $program_budget)
      ->condition('field_gd_program_goal', $program_goal);
    $results = $query->execute();

    if (!empty($results)) {
      foreach ( $results as $result) {
        $paragraph = \Drupal\paragraphs\Entity\Paragraph::load($result);
        $parent= $paragraph->getParentEntity();

        // First filter our any orphaned paragraph entities.
        if (isset($parent)) {
          $parent_entity_type = $parent->getEntityTypeId();

          // Get the parent's type, based on what entity type it is.
          if ( $parent_entity_type == 'taxonomy_term') {
            $parent_type = $parent->getVocabularyId();
          } elseif ($parent_entity_type == 'node') {
            $parent_type = $parent->getType();
          } else {
            return; // Todo add error login
          }

          // If the parent's type matches the $type parameter, return the parent's ID
          if ($parent_type == $type) {

            // Make sure this isn't an orphaned paragraph
            $parent_mappings  = $parent->get('field_givingdata_conversions')->getValue();
            foreach($parent_mappings as $mapping) {
              if ($paragraph->getRevisionId() == $mapping['target_revision_id']) {
                  return $parent->id();
              };
            }
          }
        }
      }
    }

    // Error handling if nothing was found.
    $program_goal_name = $this->find_term_name_from_id($program_goal);
    $program_budget_name = $this->find_term_name_from_id($program_budget);
    $error_message = new TranslatableMarkup("No $type found that relates to a Program Budget value
    of <strong>$program_budget_name</strong> and a Program Goal value of <strong>$program_goal_name</strong>.");
    \Drupal::messenger()->addWarning($error_message);
    throw new MigrateException($error_message);
  }
}

