<?php

namespace Drupal\rbf_givingdata\Plugin\migrate\source;

// use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\Url;
use Drupal\migrate\Row;

/**
 * Not currently in active use, kept here for potential future buildout.
 *
 * @MigrateSource(
 *   id = "grant_api"
 * )
 */
class GrantAPI extends Url{

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row)
  {
    return parent::prepareRow($row);
  }
}
