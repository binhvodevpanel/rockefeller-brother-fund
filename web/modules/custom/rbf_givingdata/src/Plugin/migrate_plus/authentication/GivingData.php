<?php

namespace Drupal\rbf_givingdata\Plugin\migrate_plus\authentication;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate_plus\AuthenticationPluginBase;
use Jumbojett\OpenIDConnectClient;

/**
 * Provides authentication process for GivingData's OpenId Connect system.
 *
 * @Authentication(
 *   id = "GivingData",
 *   title = @Translation("GivingData")
 * )
 */
class GivingData extends AuthenticationPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationOptions() {
    $oidc = new OpenIDConnectClient('https://rbf.givingdata.com',
                                'GivingDataPublishedGrants',
                                'grantsdata');
    $oidc->providerConfigParam(['token_endpoint' => 'https://rbf.givingdata.com/identity/core/connect/token']);
    $oidc->addScope(['openid', 'email', 'GivingDataApi']);

    // Add username and password.
    $oidc->addAuthParam(['username' => 'jeff@devcollaborative.com']);
    $oidc->addAuthParam(['password' => 'G&CB$xw8Hd0G']);

    // Perform the auth and return the token (to validate check if the access_token property is there and a valid JWT) :
    $token = $oidc->requestResourceOwnerToken(TRUE)->access_token;

    return [
      'headers' => [
        'Authorization' => 'Bearer ' . $token,
        'Accept' => 'application/json; charset=utf-8',
        'Content-Type' => 'application/x-www-form-urlencoded'
      ],
    ];
  }

}
