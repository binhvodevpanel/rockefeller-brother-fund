<?php

/**
 * Default controller for the miniorange_2fa module.
 */

namespace Drupal\miniorange_2fa\Controller;

use Drupal\Core\Form\formBuilder;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

class miniorange_2faController extends ControllerBase {
    protected $formBuilder;
    public function __construct(FormBuilder $formBuilder) {
        $this->formBuilder = $formBuilder;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get("form_builder")
        );
    }

  /**
   * @return Response
   */
    public function openModalForm() {
        $response = new AjaxResponse();
        $modal_form = $this->formBuilder->getForm('\Drupal\miniorange_2fa\Form\MoAuthRemoveAccount');
        $response->addCommand(new OpenModalDialogCommand('Remove Account', $modal_form, ['width' => '800'] ) );
        return $response;
    }
}