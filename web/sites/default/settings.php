<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all environments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to ensure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * Place the config directory outside of the Drupal root.
 */
$settings['config_sync_directory'] = dirname(DRUPAL_ROOT) . '/config';

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}

/**
 * Always install the 'standard' profile to stop the installer from
 * modifying settings.php.
 */
$settings['install_profile'] = 'mothership';

// Set environment indicator for non-production instances.
if (!(isset($_ENV['PANTHEON_ENVIRONMENT']) && $_ENV['PANTHEON_ENVIRONMENT'] === 'live')) {
  $config['environment_indicator.indicator']['bg_color'] = '#f6734d';
  $config['environment_indicator.indicator']['fg_color'] = '#474442';
}

// Excluded development modules from config sync.
$settings['config_exclude_modules'] = ['devel', 'kint','devel_php'];

$settings['hash_salt']="sdfwerndsf";
/**
 * There are some basic configuration created by DevPanel
 */
$databases['default']['default'] = [
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USER'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST'),
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];

$settings['memcache']['servers'] = ['dp-mem-610a8e8d5322530014636881.devpanel-1616746222093zyuzqk.svc.cluster.local:11211' => 'default'];
$settings['memcache']['bins'] = ['default' => 'default'];
$settings['memcache']['key_prefix'] = 'uah1297dfadsfd';
$settings['cache']['default'] = 'cache.backend.memcache';
$settings['cache']['bins']['render'] = 'cache.backend.memcache';

# S3FS setting
$config['s3fs.settings']['bucket'] = 'devpanel-cluster-vvybhkeh';
$config['s3fs.settings']['public_folder'] = 'testing-1636599098890';
$config['s3fs.settings']['region'] = 'us-west-2';
$config['s3fs.settings']['use_https'] = FALSE;

$settings['s3fs.access_key'] = 'AKIAUJYT34E35LOSSYB5';
$settings['s3fs.secret_key'] = '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';

$settings['s3fs.use_s3_for_public'] = TRUE;
# $settings['s3fs.upload_as_private'] = TRUE;
