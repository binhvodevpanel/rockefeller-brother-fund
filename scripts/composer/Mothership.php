<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer\Mothership;

use Symfony\Component\Yaml\Yaml;

class ScriptHandler
{
  /**
   * Helper function for buildProfile() and installLightship(). Outputs items in
   * an ordered list.
   *
   * @param [string] $step_description
   * @return void
   */
  protected static function displayStep ($step_description) {
    global $step_number;
    $step_number += 1;
    echo $step_number . ') ' . $step_description;
    echo "\n";
  }

  /**
   * This script migrates the contents of the config sync directory to the
   * mothership installation profile and removes the sync data that prevents
   * usability during the site installation process.
   */
  public static function buildProfile() {

    $profile_path = './web/profiles/mothership';
    $profile_config_path = $profile_path . '/config/install';
    $config_path = './config';
    $profile_name = 'mothership';
    $profile_info_file = $profile_path . '/' . $profile_name . '.info.yml';
    $step_number = 0;

    static::displayStep ('Emptying profile config directory.');
    shell_exec('rm ' . $profile_config_path . '/* -r');

    static::displayStep ('Running config export.');
    shell_exec('lando drush cex -y');

    static::displayStep ('Migrating config.');
    shell_exec('cp ' . $config_path . '/*.yml ' . $profile_config_path);

    static::displayStep ('Stripping UUIDs and hashes.');
    shell_exec('find ' . $profile_config_path . " -type f -exec sed -i -e '/^uuid: /d' {} \;");
    shell_exec('find ' . $profile_config_path . " -type f -exec sed -i -e '/_core:/,+1d' {} \;");

    // Now migrate module data in core.extension.yml into the profile info file.
    static::displayStep ('Updating module data in the profile info file.');
    $profile_info_data = Yaml::parseFile($profile_info_file);
    $config_core_extension = Yaml::parseFile($profile_config_path . '/core.extension.yml');

    $config_module_array = $config_core_extension['module'];
    $module_list = [];
    foreach ($config_module_array as $key=>$value) {
      // Add all but the profile itself as a dependency.
      if ($key !== $profile_name) {
        $module_list[] = $key;
      }
    }

    $config_theme_array = $config_core_extension['theme'];
    $theme_list = [];
    foreach ($config_theme_array as $key=>$value) {
        $theme_list[] = $key;
    }

    $profile_info_data['install'] = $module_list;
    $profile_info_data['themes'] = $theme_list;

    file_put_contents($profile_info_file, Yaml::dump($profile_info_data, 99, 2));
    shell_exec('rm ' . $profile_config_path.'/core.extension.yml');

    echo 'Process complete. Check output above for issues.
    ';
  }

/**
 * This script installs the latest version of mothership and converts it into
 * a custom theme for the project.
 */
  public static function installLightship() {
    /*
     *  Helper function to prompt the user for input.
     */
    function get_input($prompt, $default = null) {
      echo $prompt;
      if ($default) {
        echo ' (default is "' . $default . '")';
      }
      echo ":\n";
      // echo "\n";
      $input =  rtrim(fgets(STDIN));
      if ($input) {
      return $input;
      } else {
        echo "Using default. \n";
        return $default;
      }
    }

    /*
     *  Helper function to search for files recursively.
     */
    function glob_recursive($pattern, $flags = 0) {
      $files = glob($pattern, $flags);

      foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
      {
          $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
      }

      return $files;
    }

    $step_number = 0;

    /* User prompts
    ==========================*/
    $display_name = get_input('Enter the Display Name for the theme', "Lightship");

    // Convert Display name to default machine name.
    $default_machine_name = strtolower(str_replace(' ', '_', $display_name));

    $machine_name = get_input('Enter the machine name for the theme', $default_machine_name);

    $description = get_input('Enter the custom description', 'A custom theme created by DevCollaborative, ' . date("Y") . '.');

    /* Installation process
    ==========================*/

    $theme_path = 'web/themes/custom/' . $machine_name;
    static::displayStep('Cloning lightship to ' . $theme_path . '.');
    shell_exec('git clone https://git.drupalcode.org/project/lightship.git ' . $theme_path);

    static::displayStep('Removing git repo from theme.');
    shell_exec('rm -rf '. $theme_path . '/.git');

    static::displayStep('Renaming main theme files.');
    $theme_file_list = glob($theme_path . '/lightship.*');

    foreach($theme_file_list as $file) {
      $new_filename = str_replace('lightship', $machine_name, $file);
      rename($file, $new_filename);
    }

    static::displayStep('Searching all file contents for "lightship" and replacing with "' . $machine_name . '".');
    $all_themes_files = glob_recursive($theme_path . '/*');
    foreach($all_themes_files as $file) {
      if (!is_dir($file) && $file !== $theme_path . '/README.txt') {
        $content = file_get_contents($file);
        $revised_content = str_replace('lightship', $machine_name, $content);
        file_put_contents($file, $revised_content);
      }
    }

    static::displayStep('Rewriting info file.');
    $info_file_path = $theme_path . '/' . $machine_name . '.info.yml';
    $info_data = Yaml::parseFile($info_file_path);
    $info_data['name'] = $display_name;
    $info_data['description'] = $description;
    $yaml_info_data = Yaml::dump($info_data, 99, 2);
    // Remove unecessary single quote string wrappers.
    $yaml_info_data = str_replace("'", '', $yaml_info_data);
    file_put_contents($info_file_path, $yaml_info_data);
  }
}
