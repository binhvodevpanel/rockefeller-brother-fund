/**
 * @file
 * Seetings for Accessible Mega Menu js.
 */

(function($, Drupal, drupalSettings) {
  Drupal.behaviors.rockefeller20megamenu = {
    attach: function(context, settings) {

      /**
       * Accessible Mega Menu
       * https://adobe-accessibility.github.io/Accessible-Mega-Menu/
       */

      $("#block-mainnavigation").accessibleMegaMenu({
        /* prefix for generated unique id attributes, which are required
           to indicate aria-owns, aria-controls and aria-labelledby */
        uuidPrefix: "accessible-megamenu",

        /* css class used to define the megamenu styling */
        menuClass: "accessible-megamenu",

        /* css class for a top-level navigation item in the megamenu */
        topNavItemClass: "accessible-megamenu-top-nav-item",

        /* css class for a megamenu panel */
        panelClass: "accessible-megamenu-panel",

        /* css class for a group of items within a megamenu panel */
        panelGroupClass: "accessible-megamenu-panel-group",

        /* css class for the hover state */
        hoverClass: "hover",

        /* css class for the focus state */
        focusClass: "focus",

        /* css class for the open state */
        openClass: "open"
      });


      // // Tweak so that the megamenu doesn't show flash of css animation after the page loads.
      // setTimeout(function () {
      //   $('body').removeClass('init');
      // }, 500);


       // Add column wrapper class to menu item(s) parent for theming.
      $('.megamenu .menu-level-1').each(function(){
        if ($(this).find('.accessible-megamenu-panel-group')){
          $(this).addClass('mega-menu__column-wrapper');
        }
      });

      // Add span wrapper to top menu item(s) for theming.
      $('.megamenu .accessible-megamenu-top-nav-item > a').each(function(){
        $(this).wrapInner('<span></span>');
      });

      // Add promo wrapper class to menu-item element for theming.
      $('.megamenu .field--name-field-promo-image').each(function(){
        $(this).parent().parent().addClass('menu-item__promo')
      });

      // Add small thumbnail wrapper class to menu element for theming.
      $('.megamenu .field--name-field-small-thumbnail').each(function(){
        $(this).parent().parent().addClass('menu-item__small-thumbnail')
        $(this).parent().parent().parent().addClass('menu__small-thumbnail')
      });

      // Add horizontal rule wrapper class to menu element for theming.
      $('.megamenu .horizontal-rule').each(function(){
        $(this).parent().addClass('menu-item__horizontal-rule')
      });

    }
  };
})(jQuery, Drupal, drupalSettings);
