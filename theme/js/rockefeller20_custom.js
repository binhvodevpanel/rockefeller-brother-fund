/**
 * @file
 * Code for custom js.
 */

(function($, Drupal, drupalSettings) {
  Drupal.behaviors.rockefeller20 = {
    attach: function(context, settings) {

      /**
       * Clone Utility Menu for mobile theming.
       */
      if ($('.accessible-megamenu .region-utility').length == 0) {
        $('.region-utility').once().clone().appendTo('.accessible-megamenu');
      }


      /**
       * Append Image Credit text to Feature Image Caption.
       */
      $('.feature').each(function(){
        // Append image credit (preceded by a space).
        if ($(this).find('.field--name-field-featured-image-caption').length) {
          $(this).find('.field--name-field-featured-image-caption p').append(' ').append($(this).find('.field--name-field-image-credit p').html());
          // Remove original image credit.
          $(this).find('.field--name-field-image-credit p').detach();
        }
      });

      $('.image-wrapper').each(function(){
        if ($(this).find('.field--name-field-featured-image-caption').length) {
          // Append image credit (preceded by a space).
          $(this).find('.field--name-field-featured-image-caption p').append(' ').append($(this).find('.field--name-field-image-credit p').html());
          // Remove original image credit.
          $(this).find('.field--name-field-image-credit p').detach();
        }
      });


      /**
       * Append Image Credit text to Pocantico Body Image Caption.
       */
      $('.page-node-type-pocantico-homepage .section-2').each(function(){
        if ($(this).find('.field--name-field-body-image-caption').length) {
          // Append image credit (preceded by a space).
          $(this).find('.field--name-field-body-image-caption p').append(' ').append($(this).find('.field--name-field-image-credit p').text());
          // Remove original image credit.
          $(this).find('.field--name-field-image-credit p').detach();
        }
      });


      /**
       * Append Image Credit text to Image with Caption entities.
       */
      $('.paragraph--type--large-image').each(function(){
        // Append image credit (preceded by a space).
        if ($(this).find('.field--name-field-caption').length) {
          $(this).find('.field--name-field-caption p').append(' ').append($(this).find('.field--name-field-image-credit p').html());
          // Remove original image credit.
          $(this).find('.field--name-field-image-credit p').detach();
        }
      });


      /**
       * Append Image Credit text to Image Gallery entities.
       */
      $('.paragraph--type--image.paragraph--view-mode--image-gallery').each(function(){
        // Append image credit (preceded by a space).
        if ($(this).find('.field--name-field-caption').length) {
          $(this).find('.field--name-field-caption p').append(' ').append($(this).find('.field--name-field-image-credit p').html());
          // Remove original image credit.
          $(this).find('.field--name-field-image-credit p').detach();
        }
      });

      /**
       * Add "active" class to People Listing page buttons based on query string.
       */
      // Get current page URL and split it into segments.
      let segments = window.location.href.split( '/' );
      // Split the 4th segment into path and query string variables.
      let path_and_query = segments[3].split( '?' );
      let slash_path_and_query = '/' + segments[3];
      let path = path_and_query[0];
      // If the path is "people" then look at the quick links.
      if (path == "people") {
        $('.quick-links a.tag').each(function(){
          let link = $(this).attr('href');
          // If the quick link URL is the same as the page URL, add "active" class.
          if (link == slash_path_and_query) {
            $(this).addClass('active');
          }
        });
      }


      /**
       * Toggle for Grantee Detail page Grants list.
       */
      let grant_list = $('.view-id-listing_page_grants.view-display-id-block_2');
      let grant_toggle = grant_list.find('.list-toggle');

      grant_list.addClass('processed closed');
      grant_toggle.addClass('processed closed');

      grant_toggle.on('click touchstart', function(e) {
        e.stopPropagation;
        if (grant_list.hasClass('closed')) {
          e.preventDefault();
          grant_list.removeClass('closed');
          grant_toggle.removeClass('closed');
        } else {
          e.preventDefault();
          grant_list.addClass('closed');
          grant_toggle.addClass('closed');
        }
      });


      /**
       * Toggle for Grants Search page facet filters.
       */
      if ($('.path-grants-search').once().length) {

        let facets_outer = $('aside.layout-sidebar-first');
        let facets_inner = facets_outer.find('.region-sidebar-first');
        facets_outer.before('<a href="#" class="gs-facets-toggle">Filter your search</a>');
        facets_outer.after('<div class="gs-facets-backdrop"></div>');
        facets_inner.prepend('<a href="#" class="gs-facets-close">Close filters</a>');
        let facets_toggle = $('.gs-facets-toggle');
        let facets_backdrop = $('.gs-facets-backdrop');
        let facets_close = $('.gs-facets-close');
        let facets_items = $([facets_toggle[0], facets_inner[0], facets_backdrop[0], facets_close[0]]);

        facets_items.addClass('processed closed');

        facets_toggle.on('click touchstart', function(e) {
          e.stopPropagation;
          if (facets_inner.hasClass('closed')) {
            e.preventDefault();
            facets_items.removeClass('closed');
          } else {
            e.preventDefault();
            facets_items.addClass('closed');
          }
        });

        facets_close.on('click touchstart', function(e) {
          e.stopPropagation;
          e.preventDefault();
          facets_items.addClass('closed');
          facets_toggle.focus();
        });
      }


    }
  };
})(jQuery, Drupal, drupalSettings);
