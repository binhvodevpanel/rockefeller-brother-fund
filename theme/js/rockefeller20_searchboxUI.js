/**
 * @file
 * Code for SearchBoxUI js.
 */

(function($, Drupal, drupalSettings) {
  Drupal.behaviors.rockefeller20_searchbox = {
    attach: function(context, settings) {

      /**
       * Searchbox UI functionality.
       */
      let searchBoxUI = $('.region-utility .block-views-exposed-filter-blocksite-search-page-1');
      let searchBoxSubmit = searchBoxUI.find('input.form-submit');
      let searchBoxInput = searchBoxUI.find('#edit-search-api-fulltext');
      let inputValue = searchBoxInput.val();

      // console.log(inputValue);

      function closeSearchBox() {
        if (!searchBoxUI.hasClass('closed') && (typeof inputValue === 'undefined')) {
          // Do nothing.
        }
        else if (!searchBoxUI.hasClass('closed') && (!inputValue)) {
          searchBoxUI.addClass('closed');
        }
      }

      searchBoxInput.attr('size', '');
      searchBoxUI.addClass('processed');

      // Add 'closed' class if inputValue is not 'undefined' (e.g. Search page).
      if (typeof inputValue === 'undefined') {
        // Do nothing.
      }
      else {
        searchBoxUI.addClass('closed');
      }

      // Open searchbox when focus goes into it.
      searchBoxInput.on('focusin', function(e) {
        e.preventDefault();
        if (searchBoxUI.hasClass('closed')) {
          searchBoxUI.removeClass('closed');
        }
      });

      // Close searchbox when focus leaves, unless there is an input value.
      searchBoxUI.on('focusout', function(e) {
        closeSearchBox();
      });

      // Close searchbox when esc is pressed.
      searchBoxInput.on('keydown', function(e) {
        e.stopPropagation;
        if (e.key === 'Escape') {
          searchBoxUI.addClass('closed');
          searchBoxSubmit.focus();
        } else if (e.key === 'Tab') {
          closeSearchBox();
        }
      });

      // Store value of text field.
      searchBoxInput.on('keyup', function(e) {
        inputValue = $(this).val();
      });

      // Basically the same as the two above except for clicks.
      searchBoxUI.on('click touchstart', function(e) {
        e.stopPropagation;
        if (searchBoxUI.hasClass('closed')) {
          e.preventDefault();
          searchBoxUI.removeClass('closed');
          searchBoxInput.focus();
        }
      });

    }
  };
})(jQuery, Drupal, drupalSettings);
